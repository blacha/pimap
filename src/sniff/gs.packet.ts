/** Packet types recieved from the D2 Game server */
export enum GameServerPacket {
    GameLoading = 0x00,
    GameLogonReceipt = 0x01,
    GameLogonSuccess = 0x02,
    LoadAct = 0x03,
    LoadDone = 0x04,
    UnloadDone = 0x05,
    GameLogoutSuccess = 0x06,
    MapAdd = 0x07,
    MapRemove = 0x08,
    AssignWarp = 0x09,
    RemoveGroundUnit = 0x0A,
    GameHandshake = 0x0B,
    NPCGetHit = 0x0C,
    PlayerStop = 0x0D,
    GameObjectModeChange = 0x0E,
    PlayerMove = 0x0F,
    PlayerMoveToUnit = 0x10,
    ReportKill = 0x11,
    SetGameObjectMode = 0x14,
    PlayerReassign = 0x15,
    //UNKNOWN               = 0x18,    //TODO: GS: 15   18 5d 00 16 80 21 40 0b c0 f1 b1 a8 f0 ff 07
    SmallGoldAdd = 0x19,
    ByteToExperience = 0x1A,
    WordToExperience = 0x1B,
    DWordToExperience = 0x1C,
    AttributeByte = 0x1D,
    AttributeWord = 0x1E,
    AttributeDWord = 0x1F,
    StateNotification = 0x20,
    UpdatePlayerSkill = 0x21,
    UpdatePlayerItemSkill = 0x22,
    PlayerAssignSkill = 0x23,
    GameMessage = 0x26,
    NPCInteraction = 0x27, //TODO: parse Merc Info, split in two classes / events: TownFolkInteract and MercInfo.
    PlayerQuestInfo = 0x28,
    UpdateGameQuestLog = 0x29,
    TransactionComplete = 0x2A,
    PlaySound = 0x2C,
    UpdateContainerItem = 0x3E,
    UseStackableItem = 0x3F,
    PlayerClearCursor = 0x42,
    Relator1 = 0x47,
    Relator2 = 0x48,
    UnitUseSkillOnTarget = 0x4C,
    UnitUseSkill = 0x4D,
    MercForHire = 0x4E,
    MercForHireListStart = 0x4F,
    //UNKOWN				= 0x50,	//TODO: length = 15
    GameObjectAssign = 0x51,
    PlayerQuestLog = 0x52, //TODO: figure out state values...
    PartyRefresh = 0x53,
    PlayerAssign = 0x59,
    PlayerInfomation = 0x5A,
    PlayerInGame = 0x5B,
    //UNKOWN				= 0x5D,	//TODO: length = 6
    //UnknownGame			= 0x5E,
    //UnknownGame			= 0x5F,	//TODO: Part of join data, after GameHandshake... 5f 01 00 00 00
    PortalInfo = 0x60,
    OpenWaypoint = 0x63,
    PlayerKillCount = 0x65,
    NpcMove = 0x67,
    NpcMoveToTarget = 0x68,
    NpcUpdate = 0x69, // CompStateUpdate
    NpcAction = 0x6B,
    NpcAttack = 0x6C, //MonsterAttack
    NpcStop = 0x6D,
    AboutPlayer = 0x75,
    OverHeadClear = 0x76,
    UpdateItemUI = 0x77,
    AcceptTrade = 0x78,
    GoldTrade = 0x79,
    SummonAction = 0x7A,
    AssignSkillHotkey = 0x7B,
    UseSpecialItem = 0x7C,	//TODO: Only type 4 : Identify / portal tome / scroll is known
    //UnknownGame			= 0x7E,	//TODO: Part of join data, after PlayerReassign... 7e 10 00 00 34
    PartyMemberUpdate = 0x7F,
    MercAssignment = 0x81, //TODO: INCOMPLETE
    PortalOwnership = 0x82,
    NpcWantsInteract = 0x8A,
    PlayerPartyRelationship = 0x8B,
    PlayerRelationship = 0x8C,
    AssignPlayerToParty = 0x8D,
    CorpseAssign = 0x8E,
    Pong = 0x8F,
    PartyMemberPulse = 0x90,
    //UNKNOWN				= 0x91, // Length = 26. At the start of map unload data... 91 00 93 00 ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
    SkillsLog = 0x94,
    PlayerLifeManaChange = 0x95,
    WalkVerify = 0x96,
    //UNKNOWN				= 0x97,	// Length = 1. Part of join data, towards the end. Initialise / trigger something ?
    //UNKNOWN				= 0x9B,	// Merc related... res a rogue merc in act 3 : 9b ff ff 00 00 00 00
    ItemWorldAction = 0x9C,
    ItemOwnedAction = 0x9D,
    MercAttributeByte = 0x9E,
    MercAttributeWord = 0x9F,
    MercAttributeDWord = 0xA0,
    DelayedState = 0xA7,
    SetState = 0xA8,
    EndState = 0xA9,
    AddUnit = 0xAA,
    NPCHeal = 0xAB,
    NPCAssign = 0xAC,
    WardenCheck = 0xAE,
    RequestLogonInfo = 0xAF,
    GameOver = 0xB0,

    Invalid = 0xB1,
    Wrapper = 0xF00,

    GainExperience = 0xF01,
    PlayerAttribute = 0xF02,
    ItemAction = 0xF03,
    MercAttributeNotification = 0xF04,
    UpdateQuestInfo = 40,
    InformationMessage = 90,
    QuestItemState = 93,
    SwitchWeaponSet = 151,
    ItemTriggerSkill = 153,
    MercByteToExperience = 0xa1,
    MercWordToExperience = 0xa2,
    IdentifyItem = 0xa5,
    MercGainExperience = 3845
}
